import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
const targetJson = import('./target.json');
import { Layout, Tabs, Input, Card, Popover, Col, Row, Icon, Menu, PageHeader, Transfer, Modal } from 'antd';
import { Button } from 'antd/lib/radio';
import Axios from "axios";
import AxiosFormData from "./helpers/AxiosFormData";

const { Search } = Input;
const { Header, Footer, Sider, Content } = Layout;
const { TabPane } = Tabs;
const { SubMenu } = Menu;

class App extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    mockData: [],
    targetKeys: [],
    newCaseModal: false,
    searchBar: '',
  };

  componentDidMount() {
    this.getMock();
  }

  getMock = () => {
    let targetKeys = [];
    let mockData;
    let formData = new AxiosFormData({
      '_s': this.state.searchBar,
    });

    Axios.post('http://dev.inquest/getTarget.php', formData).then((res) => {
      if (res.data != null) {
        arr = res.data.map(i => {
          console.log(i);
        });
      }
    });
    this.setState({ mockData, targetKeys });
  }

  handleChange = targetKeys => {
    this.setState({ targetKeys });
  }

  renderFooter = () => {
    return false;
  }

  showModal = () => {
    this.setState({
        newCaseModal: true
    });
  }

  handleOk = e => {
      this.setState({
          newCaseModal: false
      });
  }

  handleCancel = e => {
      return this.handleOk(e);
  }

  render() {
    return <Layout>
      <Header style={{ color: 'white' }}>
        <Row>
          <Col span={6}>
            <img src="http://localhost/logo.svg" width="150" style={{marginTop:5, textAlign:'left'}} />
          </Col>
          <Col span={12} />
          <Col span={6} style={{textAlign:'right'}}>
            <Popover placement="bottomRight" content={
              <Menu
                style={{width: 250, border:'none'}}
              >
                <div>
                  <Button type="link" style={{display:'block', width:'100%', border:'none'}}><Icon type="setting" style={{border:'none'}} /> Configurações</Button>
                  <Button type="link" style={{display:'block', width:'100%', border:'none'}}><Icon type="logout" style={{border:'none'}} /> Logout</Button>
                </div>
              </Menu>
            }>
              <Button style={{marginTop:15, height:34, paddingTop:10}}><span><Icon type="menu" /></span></Button>
            </Popover>
          </Col>
        </Row>
      </Header>
      <Layout>
        <Sider theme="dark">
          <Search style={{margin:'0 5% 20px 5%', width:'90%'}} placeholder="search" onSearch={value => console.log(value)} enterButton />
          <Menu
            defaultSelectedKeys={[]}
            defaultOpenKeys={['sub1']}
            mode="inline"
            theme="dark"
            style={{maxWidth:300}}
          >
            <Menu.Item key="1" onClick={() => {this.showModal()}}>
                <Icon type="plus" />
                <span>Novo caso</span>
            </Menu.Item>
            <SubMenu
                key="sub1"
                title={
                <span>
                    <Icon type="container" theme="filled" />
                    <span>Casos</span>
                </span>
                }
            >
              <Menu.Item key="c1">Caso 1</Menu.Item>
              <Menu.Item key="c2">Caso 2</Menu.Item>
              <Menu.Item key="c3">Caso 3</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Content>
          <Card style={{margin:20, height:'100%', padding:20, overflow:'hidden'}}>
            <Tabs defaultActiveKey="2" onChange={() => {}} style={{height:42, overflow:'visible'}}>
              <TabPane tab="Crono" key="1">
                <PageHeader
                  style={{
                    border: '1px solid rgb(235, 237, 240)',
                    marginBottom:15
                  }}
                  title="Crono"
                  subTitle="Edit"
                />
              </TabPane>
              <TabPane tab="Ecossistema" key="2">
                <PageHeader
                  style={{
                    border: '1px solid rgb(235, 237, 240)',
                    marginBottom:15
                  }}
                  title="Ecossistema"
                  // subTitle=""
                />
                <Search style={{marginBottom:20, width:'100%'}} placeholder="search"
                  onSearch={value => this.setState({searchBar: value}, this.getMock )}
                  onChange={e => {
                    let { searchBar } = this.state;
                    this.setState({searchBar});
                  }}
                  
                  enterButton
                />
                <Transfer
                  dataSource={this.state.mockData}
                  showSearch
                  showSelectAll={true}
                  style={{textAlign: 'left'}}
                  listStyle={{
                    width: '48%',
                    height: 600,
                  }}
                  // operations={['--', '--']}
                  targetKeys={this.state.targetKeys}
                  onChange={this.handleChange}
                  render={item => <span><b>{item.title}</b>: {item.description}</span>}
                  footer={this.renderFooter}
                />
              </TabPane>
              <TabPane tab="Insights" key="3">
                <PageHeader
                  style={{
                    border: '1px solid rgb(235, 237, 240)',
                    marginBottom:15
                  }}
                  title="Insights"
                  subTitle="Edit"
                />
              </TabPane>
              <TabPane tab="Coleta" key="4">
                <PageHeader
                  style={{
                    border: '1px solid rgb(235, 237, 240)',
                    marginBottom:15
                  }}
                  title="Coleta"
                  subTitle="Edit"
                />
              </TabPane>
              <TabPane tab="Kraken" key="5">
                <PageHeader
                  style={{
                    border: '1px solid rgb(235, 237, 240)',
                    marginBottom:15
                  }}
                  title="Kraken"
                  subTitle="Edit"
                />
              </TabPane>
              <TabPane tab="Monitoramento" key="6">
                <PageHeader
                  style={{
                    border: '1px solid rgb(235, 237, 240)',
                    marginBottom:15
                  }}
                  title="Monitoramento"
                  subTitle="Edit"
                />
              </TabPane>
            </Tabs>
          </Card>
        </Content>
      </Layout>
      <Footer style={{ color: 'white', background:'#4a4a4a', textAlign:'center' }}>2019 © Inquest. Todos os direitos reservados.</Footer>
      <Modal
        title="Cadastrar caso"
        visible={this.state.newCaseModal}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <p>Formulário de cadastro de caso...</p>
      </Modal>
    </Layout>;
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
