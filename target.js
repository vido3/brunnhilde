targetJson = 
{
    "target": {
        "name": "Sidnei Ribeiro Rodrigues",
        "cpf": "408.404.035-50",
        "born_date": "1992-03-08",
        "business_parteners": [{}],
        "companies": [
            {
                "name": "S2R Tech Ltda",
                "cnpj": "29.936.453/0001-06",
                "opening_date": "2018-02-28",
                "status": "active"
            },
            {
                "name": "S2R Invest Found Ltda",
                "cnpj": "17.936.453/0001-06",
                "opening_date": "2018-02-28",
                "status": "active"
            },
        ],
        "family": [
            {
                "name": "Sidnei Rodrigues",
                "relationship": "father",
            },
            {
                "name": "Cristina Lopes Ribeiro",
                "relationship": "mother",
            },
            {
                "name": "Sérgio Ribeiro Rodrigues",
                "relationship": "brother",
            },
        ],
    }    
}